package org.salac.gd.test;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.salac.gd.entity.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ApplicationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testInitialState() {
        Assertions.assertThat(
            this.restTemplate.getForObject("http://localhost:" + port + "/", Review[].class)
        ).isEqualTo(new Review[]{});
    }

    @Test
    public void testAddAndRetrieveReview() {
        final String TEST_NAME = "Name";
        final String TEST_MESSAGE = "Message";

        String requestJson = "{\"name\":\"" + TEST_NAME + "\", \"message\": \"" + TEST_MESSAGE + "\"}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);

        Review response = this.restTemplate.postForEntity(
            "http://localhost:" + port + "/add-review",
            entity,
            Review.class
        ).getBody();


        Assertions.assertThat(
                this.restTemplate.getForObject("http://localhost:" + port + "/", Review[].class)
        ).isEqualTo(new Review[]{response});

    }

    @Test
    public void testInvalidRequest() {

        String requestJson = "{\"name\":\"\"}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);

        ResponseEntity<String> response = this.restTemplate.postForEntity(
                "http://localhost:" + port + "/add-review",
                entity,
                String.class
        );

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    }

}
