package org.salac.gd.test;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.salac.gd.ReviewController;
import org.salac.gd.entity.RequestReview;
import org.salac.gd.entity.Review;
import org.salac.gd.service.InMemoryReviewPersistenceService;

import java.time.LocalDateTime;

public class ReviewControllerTest {

    private ReviewController controller;

    @Before
    public void before()
    {
        controller = new ReviewController(new InMemoryReviewPersistenceService());
    }

    @Test
    public void testInitialState() {
        // no reviews are added hence returned list should be empty
        Assertions.assertThat(controller.listReviews(null).toArray()).isEqualTo(new Review[]{});
    }

    @Test
    public void testAddAndRetrieveReview() {
        final String TEST_NAME = "Name";
        final String TEST_MESSAGE = "Message";
        final LocalDateTime TEST_TIME_CREATED_BEFORE_TEST = LocalDateTime.now();

        Review review = controller.addReview(new RequestReview(TEST_NAME, TEST_MESSAGE)).getBody();
        Assertions.assertThat(review).isNotNull();
        Assertions.assertThat(review.getName()).isEqualTo(TEST_NAME);
        Assertions.assertThat(review.getMessage()).isEqualTo(TEST_MESSAGE);
        Assertions.assertThat(review.getId()).isNotNull(); // Id should be provided
        Assertions.assertThat(review.getCreated()).isNotNull(); // date created should be provided
        // and should be in expected range
        Assertions.assertThat(review.getCreated()).isBetween(TEST_TIME_CREATED_BEFORE_TEST, LocalDateTime.now());

        // added review should be returned
        Assertions.assertThat(controller.listReviews(null).toArray()).isEqualTo(new Review[]{review});
    }

    @Test
    public void testFilterReviews() {
        final String TEST_NAME_1 = "Name 1";
        final String TEST_MESSAGE_1 = "Message 1";

        final String TEST_NAME_2 = "Name 2";
        final String TEST_MESSAGE_2 = "Message 2";
        final String TEST_MESSAGE_3 = "Message 2";

        Review review1 = controller.addReview(new RequestReview(TEST_NAME_1, TEST_MESSAGE_1)).getBody();
        Review review2 = controller.addReview(new RequestReview(TEST_NAME_2, TEST_MESSAGE_2)).getBody();
        Review review3 = controller.addReview(new RequestReview(TEST_NAME_2, TEST_MESSAGE_3)).getBody();

        // both review should be returned when filter by name is not used
        Assertions.assertThat(controller.listReviews(null).toArray()).isEqualTo(new Review[]{review1, review2, review3});
        // only reviews with given name should be returned
        Assertions.assertThat(controller.listReviews(TEST_NAME_1).toArray()).isEqualTo(new Review[]{review1});
        // only reviews with given name should be returned
        Assertions.assertThat(controller.listReviews(TEST_NAME_2).toArray()).isEqualTo(new Review[]{review2, review3});

        // no review should be returned as we are using name which isn't set anywhere
        Assertions.assertThat(controller.listReviews("NOT_SET").toArray()).isEqualTo(new Review[]{});
    }
}