package org.salac.gd.service;

import org.salac.gd.entity.Review;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * In memory implementation of ReviewPersistenceService
 */
@Service
public class InMemoryReviewPersistenceService implements ReviewPersistenceService{

    private final Collection<Review> reviews = new LinkedList<>();

    @Override
    public Review addReview(Review review) {
        reviews.add(review);
        return review;
    }

    @Override
    public Collection<Review> listReview(String name) {
        if (name == null) {
            return Collections.unmodifiableCollection(reviews);
        }
        return reviews.stream().filter(review -> review.getName().equals(name)).collect(Collectors.toList());
    }
}
