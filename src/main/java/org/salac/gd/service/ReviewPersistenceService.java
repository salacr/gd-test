package org.salac.gd.service;

import org.salac.gd.entity.Review;

import java.util.Collection;

/**
 * Service responsible for review persisting
 */
public interface ReviewPersistenceService {

    /**
     * Add review to storage
     * @param review which should be persisted
     * @return persisted review
     */
    Review addReview(Review review);

    /**
     * Filters review by a name
     * @param name optional argument
     * @return filtered collection of reviews
     */
    Collection<Review> listReview(String name);
}
