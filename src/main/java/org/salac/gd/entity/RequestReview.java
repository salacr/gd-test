package org.salac.gd.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Request data which describes new Review
 */
public class RequestReview {

    @NotNull(message = "Name can't be empty")
    @Size(min = 3, message = "Message must be at least 3 chars long")
    private String name;

    @NotNull(message = "Message can't be empty")
    @Size(min = 5, message = "Message must be at least 5 chars long")
    private String message;

    public RequestReview(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public RequestReview(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Transform Request object to "persisted" Review object.
     * @return persisted representation of this review
     */
    public Review asReview()
    {
        return new Review(name, message);
    }
}
