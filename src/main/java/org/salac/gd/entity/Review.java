package org.salac.gd.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * "Persisted" representation of Review
 */
@JsonDeserialize(builder = Review.Builder.class)
public class Review {

    private final UUID id;

    private final LocalDateTime created;

    private final String name;

    private final String message;

    public Review(UUID id, LocalDateTime created, String name, String message) {
        this.id = Objects.requireNonNull(id);
        this.created = Objects.requireNonNull(created);
        this.name = Objects.requireNonNull(name);
        this.message = Objects.requireNonNull(message);
    }

    public Review(String name, String message) {
        this(
            UUID.randomUUID(),
            LocalDateTime.now(),
            name,
            message
        );
    }



    public UUID getId() {
        return id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return Objects.equals(id, review.id) &&
                Objects.equals(created, review.created) &&
                Objects.equals(name, review.name) &&
                Objects.equals(message, review.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, created, name, message);
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", created=" + created +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder {

        private UUID id;

        private LocalDateTime created;

        private String name;

        private String message;

        public Builder setId(UUID id) {
            this.id = id;
            return this;
        }

        public Builder setCreated(LocalDateTime created) {
            this.created = created;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Review build() {
            return new Review(this.id, this.created, this.name, this.message);
        }
    }
}
