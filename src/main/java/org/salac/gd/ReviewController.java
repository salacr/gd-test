package org.salac.gd;

import org.salac.gd.entity.RequestReview;
import org.salac.gd.entity.Review;
import org.salac.gd.service.InMemoryReviewPersistenceService;
import org.salac.gd.service.ReviewPersistenceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;


@RestController
public class ReviewController {

    /**
     * "Database" of reviews
     */
    private final ReviewPersistenceService reviews;

    public ReviewController(ReviewPersistenceService reviews) {
        this.reviews = reviews;
    }

    /**
     * List all reviews, filtered by name if provided as an argument
     * @param name name of user on review
     * @return filtered reviews
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Collection<Review> listReviews(@RequestParam(value="name", required = false) String name) {
        return reviews.listReview(name);
    }

    /**
     * Stores review in database
     * @param requestReview request data of new review
     * @return persisted review
     */
    @RequestMapping(value = "/add-review", method = RequestMethod.POST)
    public ResponseEntity<Review> addReview(@Valid @RequestBody RequestReview requestReview) {

        Review review = requestReview.asReview();
        reviews.addReview(review);
        return new ResponseEntity<>(review, HttpStatus.CREATED);
    }
}
